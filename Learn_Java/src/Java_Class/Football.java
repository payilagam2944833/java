package Java_Class;

import Java_Practice.Interface1;

import Java_Practice.Interface2;

public class Football implements Interface1,Interface2 {
	
	
	

	@Override
	public void method2(String football_fc, String captain) {
		
		// TODO Auto-generated method stub
		
		System.out.println("football_fc : " + football_fc);
		
		System.out.println("captain : " + captain);
		
	}

	@Override
	public  void method1(String football_fc, String captain) {
		
		// TODO Auto-generated method stub
		
	    System.out.println("football_fc : " + football_fc);
		
		System.out.println("captain : " + captain);
		
	}
	
	public static void main (String [] args) {
		
		
		Football ball = new Football();
		
		ball.football();
		
		System.out.println();
		
		ball.method1("Inter Miami", "Leo Messi");
		
		System.out.println();
		
		ball.method2("Soudi Arabia", "Cristiano Ronaldo");
		
	}

	@Override
	public final void football() {
		// TODO Auto-generated method stub
		
		System.out.println("World Football Team Data");
		
	}
	

}
