package Java_Practice;

import Java_Class.Electronic_World;

public class Electronic_World1 extends Electronic_World {
	
	
	Electronic_World1(){
		
		super();
		
	}
	
	Electronic_World1(int discount ,String laptop_name ){
		
		super(15);
		System.out.println("Additional discount for laptop purchase " + discount + " " + "percentage");
		System.out.println("laptop_name :" + laptop_name);
		
	}
	
	Electronic_World1(int discount ,String laptop_name,String color ){
		
		this(20,"hp pavilion 15");
		color = "Rose gold";
		System.out.println("laptop color:" + color);
	}
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		new Electronic_World1();
		System.out.println();
		new Electronic_World1(10,"hp pavilion x360");
		System.out.println();
        new Electronic_World1(10,"hp pavilion 15","Rose gold");
	}

}
