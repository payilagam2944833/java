package Java_Practice;

import java.util.Scanner;

public class Home_work {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("enter array size");
		
		int size = sc.nextInt();
		
		int []arr = new int [size];
		
		System.out.print("enter the value of arrays");
		
	    for(int i = 0; i<size;i++) {
	    	
	    	arr[i] = sc.nextInt();
	    	
	    }
	    
	    System.out.println("Reverse Order :");
        
	    for(int i = arr.length-1;i>=0;i--) {
	    	
	    	System.out.print(" " + arr[i]);
	    }
	}

}
