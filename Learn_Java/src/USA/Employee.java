package USA;

public class Employee {
	
	private int id ;
	private String name;
	 protected String role;
	
     Employee(){
    	 System.out.println("Employee Data");
     }
	public Employee(String name, int id ,String role) {
		this.name = name;
		this.id = id;
		this.role = role;
	}
	
   void assign() {
	   
	   System.out.println("Employee name:" + name);
	   System.out.println("Employee id:" + id);
	   System.out.println("Employee role:" +role);
   }

	public static void main(String[] args) {
		
    
		Employee emp = new Employee();
		System.out.println();
		Employee emp1 = new Employee("sathish",1001,"Backend developer");
		Employee emp2 = new Employee("messi",1002,"UI/UX developer");
		Employee emp3 = new Employee ("neymer",1003,"cloud developer");
		emp1.assign();
		System.out.println();
		emp2.assign();
		System.out.println();
	    emp3.assign();
	}

	

}
