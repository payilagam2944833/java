package USA;

public  class Poorvika {
	
	int price;
	
	int discount;
	
	String name;

	
	void buy() {
		System.out.println(name);
		System.out.println("MRP :" + price);
	    System.out.println("discount value :" + (price/100*discount));
	    System.out.println("total price :" + (price - (price/100*discount) ));
	}
	

	Poorvika(){ //default constructor
		
		System.out.println("welcome to poorvika");
	}

	public Poorvika(int price) {  // Parameterized Constructor
		this(500,"calander",300);
		this.price = price;
		
	}

//	public Poorvika(String name ,int price, int discount) { // Parameterized Constructor
//		
//		this.name = name;
//		this.price = price;
//		this.discount = discount;
//		
//	}
	
	Poorvika (int special_discount, String newyear_gift ,int diwali_gift){
		
		System.out.println("special discount:" +  special_discount);
		System.out.println("new year gift :" + newyear_gift);
		System.out.println("diwali_gift :" + diwali_gift);
	}

	public Poorvika(String name, int price) { // Parameterized Constructor	
		
		this.name = name;
		this.price = price;
	}


	public static void main(String[] args) {
		
		Poorvika welcome = new Poorvika();
    	System.out.println();
       // Poorvika redmi = new Poorvika("redmi",15000,20);
        //Poorvika samsung = new Poorvika("samsung",30000,10);
        Poorvika nokia = new Poorvika("nokia",50000);
        
        Poorvika s = new Poorvika(500,"calander",300);
        //samsung.buy();
    	System.out.println();
       // redmi.buy();
    	System.out.println();
        nokia.buy();
        
        
	}

}
